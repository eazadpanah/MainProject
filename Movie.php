<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Movies by Json</title>
    <link rel="stylesheet" href="css/styles.css" type="text/css">
</head>
<body>
<table id="MYLayout" align="center">
    <tr>
        <td id="MYHeader" colspan="2" bgcolor="#b0c4de">
            <h3>Search Movies by Json in www.omdbapi.com </h3>
            <br>
            <?php echo "Movie You Choose is ". $_COOKIE['MovieName'];?>
        </td>
    </tr>
    <tr>
        <td id="MYSubHeader" colspan="2" bgcolor="#8fbc8f">
            This is SubHeader
        </td>
    </tr>
    <tr>
        <td id="MYMain">
            <form action="?" method="get">
                <input type="text" name="MovieName" placeholder="enter MovieName"><br><br>
                <input type="submit" name="search"><br><br>
            </form>
            <?php
            $MovieName = $_GET['MovieName'];
            setcookie("MovieName", "$MovieName", time() + 60 * 60 * 24 * 30); //expiration time one month
            if (!empty($MovieName)) {
                $url = "http://www.omdbapi.com/?t=$MovieName&apikey=9660afe8";
                $result = file_get_contents($url);
                $movieArrays = json_decode($result, true);

                echo "Title: " . $movieArrays['Title'] . "<br>";
                echo "year: " . $movieArrays['Year'] . "<br>";
                echo "Country:" . $movieArrays['Country'] . "<br>";
                echo "Genre:" . $movieArrays['Genre'] . "<br>";
                echo "Director:" . $movieArrays['Director'] . "<br>";
                echo "Language:" . $movieArrays['Language'] . "<br>";
                $poster = $movieArrays['Poster'];
                echo "<img src='$poster'  width='250px'>";

            }
            ?>
        </td>
        <td id="MYMenu" bgcolor="#d3d3d3">
            <?php
            include "menu";
            ?>
        </td>
    </tr>
    <tr>
        <td id="MYSubFooter" colspan="2" bgcolor="#8fbc8f">
            This is SubFooter
        </td>
    </tr>
    <tr>
        <td id="MYFooter" colspan="2" bgcolor="#b0c4de">
            This is Footer: CopyRight © phptrainee.ir All Rights Reserved.
        </td>
    </tr>
</table>
</body>
</html>