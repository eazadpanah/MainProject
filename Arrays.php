<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Movies by Json</title>
    <link rel="stylesheet" href="css/styles.css" type="text/css">
</head>
<body>
<table id="MYLayout" align="center">
    <tr>
        <td id="MYHeader" colspan="2" bgcolor="#b0c4de">
            <h3>Arrays in PHP </h3>
        </td>
    </tr>
    <tr>
        <td id="MYSubHeader" colspan="2" bgcolor="#8fbc8f">
            This is SubHeader
        </td>
    </tr>
    <tr>
        <td id="MYMain">

            <?php
            echo "<br>------------------------------------one dimentional arrays------------------------------------<br>";

            $cars = array("BMV", "Toyota", "Mazda", "MVM");
            var_dump($cars);
            echo "<br><br>";
            echo $cars[2];
            echo "<br><br>";
            foreach ($cars as $value) {
                echo "<br>" . $value;
            }

            echo "<br>------------------------------------array's pointers-------------------------------------------<br>";
            $mycar = current($cars);
            echo $mycar . "<br>";
            echo $mycar = next($cars);
            echo "<br>------------------------------------two dimentional array--------------------------------------<br>";

            $carsyear = array(
                "BMV" => "1902",
                "Toyota" => "1900",
                "Mazda" => "1915",
                "MVM" => "1915",
            );
            foreach ($carsyear as $key => $value) {
                echo $key . " " . " 's produce year is :   " . $value . "<br>";
            }

            echo "<br>-------------------------------------tree dimentional array--------------------------------------<br>";
            $multiarray = array(
                "Ali" => 65,
                "Reza" => "Ahmadi",
                452 => 896,
                "BigO" => array("Eyes" => "Optic")
            );
            var_dump($multiarray);

            echo "<br><br>------------------------------foreach for function which return array------------------------<br>";

            function get_students()
            {
                return array("Alireza", "Amir", "Sara", "Ali");
            }

            foreach (get_students() as $value) {
                echo "<br>" . $value;
            }
            ?>

        </td>
        <td id="MYMenu" bgcolor="#d3d3d3">
            <?php
            include "menu";
            ?>
        </td>

    </tr>
    <tr>
        <td id="MYSubFooter" colspan="2" bgcolor="#8fbc8f">
            This is SubFooter
        </td>
    </tr>
    <tr>
        <td id="MYFooter" colspan="2" bgcolor="#b0c4de">
            This is Footer: CopyRight © phptrainee.ir All Rights Reserved.
        </td>
    </tr>
</table>
</body>
</html>













