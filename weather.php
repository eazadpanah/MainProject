<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Yahoo Weather API Response by Json</title>
    <link rel="stylesheet" href="css/styles.css" type="text/css">
</head>
<body>
<table id="MYLayout" align="center">
    <tr>
        <td id="MYHeader" colspan="2" bgcolor="#b0c4de">
            <h3>Search Temperature of Cities by YAHOO Json </h3>
            <?php echo $_COOKIE['city']; ?>
        </td>
    </tr>
    <tr>
        <td id="MYSubHeader" colspan="2" bgcolor="#8fbc8f">
            This is SubHeader

        </td>
    </tr>
    <tr>
        <td id="MYMain">
            <form action="?" method="get">
                <input type="text" name="city" placeholder="Enter city name : "><br><br>
                <input type="submit" name="search"><br><br>
            </form>
            <?php
            $city = $_GET['city'];
            setcookie("city", "$city", time() + 60 * 60 * 24 * 30); //expiration time one month
            if (!empty($city)) {
                $url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22$city%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
                $result = file_get_contents($url);
                $weatherArrays = json_decode($result, true);
                $temp = $weatherArrays['query']['results']['channel']['item']['condition']['temp'];
                echo "temp of $city is $temp F";
                echo "<br>";
                $c = ($temp - 32) / 1.8;
                echo "temp of $city is $c C";
            }
            ?>
        </td>
        <td id="MYMenu" bgcolor="#d3d3d3">
            <?php
            include "menu";
            ?>
        </td>
    </tr>
    <tr>
        <td id="MYSubFooter" colspan="2" bgcolor="#8fbc8f">
            This is SubFooter
        </td>
    </tr>
    <tr>
        <td id="MYFooter" colspan="2" bgcolor="#b0c4de">
            This is Footer: CopyRight © phptrainee.ir All Rights Reserved.
        </td>
    </tr>
</table>
</body>
</html>

