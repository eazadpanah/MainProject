<?php

/**
 * Created by PhpStorm.
 * User: user85
 * Date: 20/01/2018
 * Time: 07:57 PM
 */

function getDB()
{
    $connection = new mysqli("localhost", "amir", "123", "fridays");
    if ($connection->connect_error) {
        return null;
    } else {
        mysqli_query($connection, "set names utf8");//for save farsi language data,send all my query utf8
        return $connection;
    }
}

class StudentManager
{
    private $name;
    private $family;
    private $city;
    private $student_number;
    private $connection;

    /**
     * StudentManager constructor.
     * @param $connection
     */
    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function register()
    {
        $name = $this->getName();
        $family = $this->getFamily();
        $student_number = $this->getStudentNumber();
        $city = $this->getCity();
        $insertQuery = "INSERT INTO students (name,family,student_number,city)
        VALUES ('$name','$family','$student_number','$city')";
        mysqli_query($this->connection, $insertQuery);
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getFamily()
    {
        return $this->family;
    }

    public function setFamily($family)
    {
        $this->family = $family;
    }

    public function getStudentNumber()
    {
        return $this->student_number;
    }

    public function setStudentNumber($student_number)
    {
        $this->student_number = $student_number;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function getStudents()
    {
        $result = mysqli_query($this->connection, "SELECT * FROM students");
        while ($row = mysqli_fetch_assoc($result)) {
            $students[] = $row;
        }
        return $students;
    }

//always write this destructor
    public function __destruct()
    {
        mysqli_close($this->connection);
    }
}

$action = $_REQUEST['action'];
if ($action == "register") {
//    $name = $_REQUEST['name'];
//    $family = $_REQUEST['family'];
//    $student_number = $_REQUEST['student_number'];
//    $city = $_REQUEST['city'];
//    $std = new StudentManager(getDB());
//    $std->setName($name);
//    $std->setFamily($family);
//    $std->setStudentNumber($student_number);
//    $std->setCity($city);

    $std = new StudentManager(getDB());
    $std->setName($_REQUEST['name']); //we can write this form instead up codes and here we call setters
    $std->setFamily($_REQUEST['family']);
    $std->setStudentNumber($_REQUEST['student_number']);
    $std->setCity($_REQUEST['city']);
    $std->register();
}
if ($action == 'list') {
    $std = new StudentManager(getDB());
    $studentsArray = $std->getStudents();
//    var_dump($std->getStudents());
    foreach ($studentsArray as $student) {
        echo $student['name']." ".$student['family']."<br>";
    }
}