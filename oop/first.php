<?php

/**
 * Created by PhpStorm.
 * User: user85
 * Date: 19/01/2018
 * Time: 09:33 PM
 */
class Car
{
    public $name;
    public $year;
    public $color;
    public $owner;

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    public function saveDB()
    {
        $color = $this->getColor();
        $name = $this->getName();
        $owner = $this->getOwner();
        $insertQuery = "INSERT INTO cars(color,name,owner)
                VALUES ('$color,$name,$owner')";
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }
}

function newCar($name, $owner, $color)
{
    $samand = new Car();
    $samand->setName($name);
    $samand->setOwner($owner);
    $samand->setColor($color);
    $samand->saveDB();
}

newCar("samand", "ali", "red");
newCar("pride", "alireza", "blue");
newCar("pride", "alireza", "blue");
newCar("pride", "alireza", "blue");