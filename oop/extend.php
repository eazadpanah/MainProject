<?php

/**
 * Created by PhpStorm.
 * User: user85
 * Date: 20/01/2018
 * Time: 07:11 PM
 */
class Human
{
    public $name ="ali (parent name)";
    public $family;
    public $nationality;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getFamily()
    {
        return $this->family;
    }

    /**
     * @param mixed $family
     */
    public function setFamily($family)
    {
        $this->family = $family;
    }

    /**
     * @return mixed
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param mixed $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

}

class Student extends Human
{
    public function getParentName()
    {
        return parent::getName();
    }
}
$std = new Student();
$std->setName("mohammad (child name)");
echo $std->getName();
echo "<br><br>";
echo $std->getParentName();


