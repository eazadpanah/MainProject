<?php
/**
 * Created by PhpStorm.
 * User: user85
 * Date: 18/01/2018
 * Time: 12:15 PM
 */
include "publics.php";
$connection = getDB();
$username = $_REQUEST['username'];
$password = $_REQUEST['password'];
//$password="' or '1'='1";

//with injection
$results = $connection->query("SELECT * FROM users WHERE username='$username' AND password='$password'");
if ($results->num_rows > 0) {
    echo "login is successful";
} else {
    echo "login is not successful";
}

echo "<br><br>";

//without injection
$query = "SELECT * FROM users WHERE username=? AND password=?";
$statement = $connection->stmt_init();
if ($statement->prepare($query)) {
    $statement->bind_param("ss", $username, $password);
    $statement->execute();
    $results_ = $statement->get_result();
    if ($results->num_rows > 0) {
        echo "injection checking login is successful";
    } else {
        echo "injection checking login is not successful";
    }

}


